part of './taro_api.dart';

class _TaroRouterApi {
  void switchTab({String url}) {
    TaroApi.global.callMethod('switchTab', [
      TaroApi.jsify({'url': url})
    ]);
  }

  void reLaunch({String url}) {
    TaroApi.global.callMethod('reLaunch', [
      TaroApi.jsify({'url': url})
    ]);
  }

  void redirectTo({String url}) {
    TaroApi.global.callMethod('redirectTo', [
      TaroApi.jsify({'url': url})
    ]);
  }

  void navigateTo({String url}) {
    TaroApi.global.callMethod('navigateTo', [
      TaroApi.jsify({'url': url})
    ]);
  }

  void navigateBack({String delta}) {
    TaroApi.global.callMethod('navigateBack', [
      TaroApi.jsify({'delta': delta})
    ]);
  }
}
