part of './taro_api.dart';

enum ToastIcon {
  none,
  success,
  loading,
}

enum AnimationTimingFunc {
  linear,
  easeIn,
  easeOut,
  easeInOut,
}

class AnimationOption {
  Duration duration;
  AnimationTimingFunc timingFunc;

  AnimationOption({this.duration, this.timingFunc});

  Map toJson() {
    return {
      'duration': duration.inMilliseconds,
      'timingFunc': (() {
        switch (timingFunc) {
          case AnimationTimingFunc.linear:
            return 'linear';
          case AnimationTimingFunc.easeIn:
            return 'easeIn';
          case AnimationTimingFunc.easeOut:
            return 'easeOut';
          case AnimationTimingFunc.easeInOut:
            return 'easeInOut';
          default:
            return 'linear';
        }
      })(),
    };
  }
}

class _TaroUIApi {
  void showToast(
      {String title,
      Duration duration,
      ToastIcon icon,
      String image,
      bool mask}) {
    TaroApi.global.callMethod('showToast', [
      TaroApi.jsify({
        'title': title,
        'duration': duration?.inMilliseconds,
        'icon': (() {
          if (icon == null) {
            return 'none';
          }
          switch (icon) {
            case ToastIcon.none:
              return 'none';
            case ToastIcon.success:
              return 'success';
            case ToastIcon.loading:
              return 'loading';
            default:
              return 'none';
          }
        })(),
        'image': image,
        'mask': mask,
      })
    ]);
  }

  void hideToast() {
    TaroApi.global.callMethod('hideToast');
  }

  void showLoading({String title, bool mask}) {
    TaroApi.global.callMethod('showLoading', [
      TaroApi.jsify({
        'title': title,
        'mask': mask,
      })
    ]);
  }

  void hideLoading() {
    TaroApi.global.callMethod('hideLoading');
  }

  Future<bool> showModal({
    Color cancelColor,
    String cancelText,
    Color confirmColor,
    String confirmText,
    String content,
    bool showCancel,
    String title,
  }) {
    Completer completer = Completer();
    TaroApi.global.callMethod('showModal', [
      TaroApi.jsify({
        'cancelColor': colorToHex(cancelColor),
        'cancelText': cancelText,
        'confirmColor': colorToHex(confirmColor),
        'confirmText': confirmText,
        'content': content,
        'showCancel': showCancel,
        'title': title,
        'success': (JsObject res) {
          if (res['cancel'] == true) {
            completer.complete(false);
          } else if (res['confirm'] == true) {
            completer.complete(true);
          } else {
            completer.complete(false);
          }
        }
      })
    ]);
    return completer.future;
  }

  Future<int> showActionSheet({
    List<String> itemList,
    Color itemColor,
  }) {
    Completer completer = Completer();
    TaroApi.global.callMethod('showActionSheet', [
      TaroApi.jsify({
        'itemColor': colorToHex(itemColor),
        'itemList': itemList,
        'success': (JsObject res) {
          if (res['tapIndex'] != null) {
            completer.complete(res['tapIndex']);
          } else {
            completer.complete(null);
          }
        },
        'fail': (JsObject res) {
          completer.complete(null);
        }
      })
    ]);
    return completer.future;
  }

  void showNavigationBarLoading() {
    TaroApi.global.callMethod('showNavigationBarLoading');
  }

  void setNavigationBarTitle(String title) {
    TaroApi.global.callMethod('setNavigationBarTitle', [
      TaroApi.jsify({'title': title})
    ]);
  }

  void setNavigationBarColor({
    Color frontColor,
    Color backgroundColor,
    AnimationOption animation,
  }) {
    TaroApi.global.callMethod('setNavigationBarColor', [
      TaroApi.jsify({
        'backgroundColor': colorToHex(backgroundColor),
        'frontColor': colorToHex(frontColor),
        'animation': animation?.toJson(),
      })
    ]);
  }

  void hideNavigationBarLoading() {
    TaroApi.global.callMethod('hideNavigationBarLoading');
  }

  void hideHomeButton() {
    TaroApi.global.callMethod('hideHomeButton');
  }

  void loadFontFace({
    bool global,
    String family,
    String source,
    String descStyle,
    String descVariant,
    String descWeight,
  }) {
    TaroApi.global.callMethod('loadFontFace', [
      TaroApi.jsify({
        'global': global,
        'family': family,
        'source': source,
        'desc': (() {
          if (descStyle != null || descVariant != null || descWeight != null) {
            return {
              'style': descStyle,
              'variant': descVariant,
              'weight': descWeight
            }..removeWhere((key, value) => value == null);
          }
        })()
      })
    ]);
  }

  void stopPullDownRefresh() {
    TaroApi.global.callMethod('stopPullDownRefresh');
  }

  void startPullDownRefresh() {
    TaroApi.global.callMethod('startPullDownRefresh');
  }

  void pageScrollTo({Duration duration, int scrollTop}) {
    TaroApi.global.callMethod('pageScrollTo', [
      TaroApi.jsify({
        'duration': duration?.inMilliseconds,
        'scrollTop': scrollTop,
      })
    ]);
  }

  void setTopBarText(String text) {
    TaroApi.global.callMethod('setTopBarText', [
      TaroApi.jsify({'text': text})
    ]);
  }

  void onKeyboardHeightChange(Function(double) callback) {
    TaroApi.global.callMethod('onKeyboardHeightChange', [
      (JsObject res) {
        callback(res['height']);
      }
    ]);
  }

  void hideKeyboard(String text) {
    TaroApi.global.callMethod('hideKeyboard');
  }

  String colorToHex(Color color) {
    if (color == null) return null;
    return '#${color.red.toRadixString(16)}${color.green.toRadixString(16)}${color.blue.toRadixString(16)}';
  }
}
