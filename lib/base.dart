part of './taro_api.dart';

class _TaroBaseApi {
  JsObject base64ToArrayBuffer(String base64) {
    return TaroApi.global.callMethod('base64ToArrayBuffer', [base64]);
  }

  String arrayBufferToBase64(JsObject buffer) {
    return TaroApi.global.callMethod('arrayBufferToBase64', [buffer]);
  }

  Future<SystemInfoResult> getSystemInfo() {
    final completer = Completer<SystemInfoResult>();
    TaroApi.global.callMethod('getSystemInfo', [
      TaroApi.jsify({
        "fail": () {
          completer.completeError('');
        },
        "success": (JsObject res) {
          completer.complete(SystemInfoResult(res));
        }
      })
    ]);
    return completer.future;
  }

  SystemInfoResult getSystemInfoSync() {
    final res = TaroApi.global.callMethod('getSystemInfoSync', []);
    return SystemInfoResult(res);
  }

  UpdateManager getUpdateManager() {
    final res = TaroApi.global.callMethod('getUpdateManager', []);
    return UpdateManager(res);
  }

  SystemInfoResult getLaunchOptionsSync() {
    final res = TaroApi.global.callMethod('getLaunchOptionsSync', []);
    return SystemInfoResult(res);
  }

  SystemInfoResult getEnterOptionsSync() {
    final res = TaroApi.global.callMethod('getEnterOptionsSync', []);
    return SystemInfoResult(res);
  }
}

class SystemInfoResult {
  final JsObject data;

  SystemInfoResult(this.data);

  String get sdkVersion => data['SDKVersion']; //客户端基础库版本

  bool get albumAuthorized => data['albumAuthorized']; //允许微信使用相册的开关（仅 iOS 有效）

  num get benchmarkLevel => data[
      'benchmarkLevel']; //设备性能等级（仅Android小游戏）。取值为：-2 或 0（该设备无法运行小游戏），-1（性能未知），>=1（设备性能值，该值越高，设备性能越好，目前最高不到50）

  bool get bluetoothEnabled => data['bluetoothEnabled']; //蓝牙的系统开关

  String get brand => data['brand']; //设备品牌

  bool get cameraAuthorized => data['cameraAuthorized']; //允许微信使用摄像头的开关

  num get fontSizeSetting =>
      data['fontSizeSetting']; //用户字体大小（单位px）。以微信客户端「我-设置-通用-字体大小」中的设置为准

  String get language => data['language']; //微信设置的语言

  bool get locationAuthorized => data['locationAuthorized']; //允许微信使用定位的开关

  bool get locationEnabled => data['locationEnabled']; //地理位置的系统开关

  bool get microphoneAuthorized => data['microphoneAuthorized']; //允许微信使用麦克风的开关

  String get model => data['model']; //设备型号

  bool get notificationAlertAuthorized =>
      data['notificationAlertAuthorized']; //允许微信通知带有提醒的开关（仅 iOS 有效）

  bool get notificationAuthorized => data['notificationAuthorized']; //允许微信通知的开关

  bool get notificationBadgeAuthorized =>
      data['notificationBadgeAuthorized']; //允许微信通知带有标记的开关（仅 iOS 有效）

  bool get notificationSoundAuthorized =>
      data['notificationSoundAuthorized']; //允许微信通知带有声音的开关（仅 iOS 有效）

  num get pixelRatio => data['pixelRatio']; //设备像素比

  String get platform => data['platform']; //客户端平台

  // safeArea	SafeAreaResult	在竖屏正方向下的安全区域
  //
  num get screenHeight => data['screenHeight']; //屏幕高度，单位px

  num get screenWidth => data['screenWidth']; //屏幕宽度，单位px

  num get statusBarHeight => data['statusBarHeight']; //状态栏的高度，单位px

  String get system => data['system']; //操作系统及版本

  String get version => data['version']; //微信版本号

  bool get wifiEnabled => data['wifiEnabled']; //Wi-Fi 的系统开关

  num get windowHeight => data['windowHeight']; //可使用窗口高度，单位px

  num get windowWidth => data['windowWidth']; //可使用窗口宽度，单位px

  String get errMsg => data['errMsg']; //调用结果
}

// todo
class UpdateManager {
  final JsObject data;

  UpdateManager(this.data);

  void applyUpdate() {}
  void onCheckForUpdate() {}
  void onUpdateFailed() {}
  void onUpdateReady() {}
}

class LaunchOptionsApp {
  final JsObject data;

  LaunchOptionsApp(this.data);

  String get path => data['path']; //	启动小程序的路径 (代码包路径)

  int get scene => data['scene']; //	启动小程序的场景值

  Map get query => data['query']; //	启动小程序的 query 参数

  String get shareTicket => data['shareTicket']; //	shareTicket，详见获取更多转发信息

  LaunchOptionsAppReferrerInfo get referrerInfo => LaunchOptionsAppReferrerInfo(
      data['referrerInfo']); //	来源信息。从另一个小程序、公众号或 App 进入小程序时返回。否则返回 {}。(参见后文注意)

  // List get forwardMaterials =>
  //     data['forwardMaterials']; //	打开的文件信息数组，只有从聊天素材场景打开（scene为1173）才会携带该参数

}

class LaunchOptionsAppReferrerInfo {
  final JsObject data;

  LaunchOptionsAppReferrerInfo(this.data);

  String get appId => data['appId']; //来源小程序、公众号或 App 的 appId

  Map get extraData => data['extraData']; //来源小程序传过来的数据，scene=1037或1038时支持

}
